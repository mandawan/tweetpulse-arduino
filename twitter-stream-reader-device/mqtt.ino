void mqttCallback(char* topic, byte* payload, unsigned int length) {
  // handle message arrived
  Serial.println("Message arrived:  topic: " + String(topic));
  Serial.println("Length: " + String(length,DEC));
  
  //Transformation en chaine de caractere
  payload[length] = '\0';
  String strPayload = String((char*)payload);

  Serial.println(strPayload);
  
  //
  int tempAverage = strPayload.substring(0, strPayload.indexOf(":")).toInt();
  Serial.print("strPayload :");
  Serial.println(strPayload);
  //String tempstringQuery = strPayload.substring(strPayload.indexOf("/./")+3, strPayload.length()+1);

  // parse response from server
  int index;
  String tempstringQuery;
  
  for (int i = 0; i < strPayload.length(); i++) {
    if (strPayload.substring(i, i+1) == ":") {
      index = strPayload.substring(0, i).toInt();
      tempstringQuery = strPayload.substring(i+1, strPayload.length()-3);
      stringLang = strPayload.substring(strPayload.length()-2, strPayload.length());
      break;
    }
  }
  Serial.println(index);
  Serial.println(tempstringQuery);
  Serial.println(stringLang);  
  
  Serial.println(strPayload);
  
  if(tempstringQuery != stringQuery)
  {
    Serial.print("la recherche a change :");
    Serial.println(tempstringQuery);

    stringQuery = tempstringQuery;
        
    mentionNum = 0;
    mentionCount = 0;
  }
  
  //ledblink_ms = (tempAverage > lastAverage) ? ledblink_short_ms : ledblink_long_ms;
  lastAverage = tempAverage;

  //launchTwitterStreaming(stringQuery, stringLang);  
  //Serial.println("MQTT message :");

  Serial.print("moyenne de mentions : ");
  Serial.println(lastAverage);

  Serial.print("recherche : ");
  Serial.println(stringQuery);

  int check_if_init = String(topic).indexOf("uzful_control");

  if (check_if_init != -1){
    Serial.println("send for init control");
  } else {
    Serial.println("launch write in /tmp");
    uploadRequest(stringQuery, stringLang);
    launchTwitterStreaming();
    
    }
  
  
  //SetTimer(sendToMQTTHours,sendToMQTTMinutes,sendToMQTTSeconds);
  //StartTimer();
}

boolean connectMQTT()
{
  Serial.println("connectMQTT");
    
  //if(!mqttClient.connect((char*)pClientId)) return false;
  if(!mqttClient.connect("F30F0A")) return false;
  
  Serial.print("mqtt connect as ");
  Serial.println(clientId);

  mqttClient.subscribe((char*)controlTopicPath); 
  //mqttClient.subscribe("uzful_data/F30F0A");
  Serial.print("subscribed to : ");
  Serial.println((char*)controlTopicPath);
  mqttClient.subscribe("uzful_data/F30F0A");
  Serial.print("subscribed to : ");
  Serial.println("uzful_data/F30F0A");

  //Serial.print("Last average : ");
  //Serial.println(lastAverage);

  //TEMP
  lastAverage = -1;
  //mqttClient.publish("uzful_control/F30F0A", "test de message de controle");
  
  //init
  if(lastAverage < 0) {
    mqttClient.publish((char*)controlTopicPath, pClientId);
    Serial.print("asked for init to server on ");
    Serial.print(controlTopicPath);
  }
  else
  {
    mqttClient.subscribe((char*)pDataTopicPath);
    Serial.println("suscribe to mqtt/pullPath ");
    //on souscrit au topic de pull classique  
  }

  return true;
}

void setAndPrintArduinoConfig(){
  Serial.println("setAndPrintArduinoConfig()");
  Process wifiCheck;  // initialize a new process

  wifiCheck.runShellCommand("/usr/bin/pretty-wifi-info.lua");  // command you want to run

  // while there's any characters coming back from the 
  // process, print them to the serial monitor:
  String wifiCo;
  while (wifiCheck.available() > 0) {
    char c = wifiCheck.read();
    wifiCo += c;
  }
  aMacAddress = wifiCo.substring(wifiCo.indexOf("MAC address: ")+13, wifiCo.indexOf("MAC address: ")+13+17);
  aLogin      = aMacAddress.substring(9, aMacAddress.length()+1) ;
  aLogin.replace(":", "");
  
  //on copie aLogin dans clientID
  aLogin.toCharArray(clientId, aLogin.length()+1);
  
  aIPAddress   = wifiCo.substring(wifiCo.indexOf("IP address: ")+12, wifiCo.indexOf("/255"));

  
  
  Serial.println("IP : " + aIPAddress);
  Serial.println("MAC : " + aMacAddress);  
  Serial.println("ARDUINO LOGIN : '" + aLogin + "'");    
  Serial.println("---------");  
  Serial.println(wifiCo);
}
