unsigned long Watch, _micro, time = micros();
unsigned int Clock = 0, R_clock;
boolean TimerReset = false, TimerStop = false, TimerPaused = false, TimerStarted = false, TimerIsFinished = false;

volatile boolean timeFlag = false;


void handleTimer(){ 

  //fin d'un timer
  if(!CountDownTimer() && TimerStarted)
  {
    TimerIsFinished = true;
    SetTimer(sendToMQTTHours,sendToMQTTMinutes,sendToMQTTSeconds); 
    Serial.println("timer finished");
  }
  // this prevents the time from being constantly shown.
  if (TimeHasChanged() ) 
  {
    
  }
}

boolean CountDownTimer()
{
  static unsigned long duration = 1000000; // 1 second
  timeFlag = false;

  if (!TimerStop && !TimerPaused) // if not Stopped or Paused, run timer
  {
    // check the time difference and see if 1 second has elapsed
    if ((_micro = micros()) - time > duration ) 
    {
      Clock--;
      timeFlag = true;

      if (Clock == 0) // check to see if the clock is 0
        TimerStop = true; // If so, stop the timer

     // check to see if micros() has rolled over, if not,
     // then increment "time" by duration
      _micro < time ? time = _micro : time += duration; 
    }
  }
  return !TimerStop; // return the state of the timer
}

void Timer()
{
  SetTimer(R_clock);
  TimerStop = false;
}

void StartTimer()
{
  Watch = micros(); // get the initial microseconds at the start of the timer
  TimerStop = false;
  TimerPaused = false;
  TimerStarted = true;
}

void StopTimer()
{
  TimerStop = true;
}

void StopTimerAt(unsigned int hours, unsigned int minutes, unsigned int seconds)
{
  if (TimeCheck(hours, minutes, seconds) )
    TimerStop = true;
}

void PauseTimer()
{
  TimerPaused = true;
}

void ResumeTimer() // You can resume the timer if you ever stop it.
{
  TimerPaused = false;
}

void SetTimer(unsigned int hours, unsigned int minutes, unsigned int seconds)
{
  // This handles invalid time overflow ie 1(H), 0(M), 120(S) -> 1, 2, 0
  unsigned int _S = (seconds / 60), _M = (minutes / 60);
  if(_S) minutes += _S;
  if(_M) hours += _M;

  Clock = (hours * 3600) + (minutes * 60) + (seconds % 60);
  R_clock = Clock;
  TimerStop = false;
  TimerIsFinished = false;
}

void SetTimer(unsigned int seconds)
{
 // StartTimer(seconds / 3600, (seconds / 3600) / 60, seconds % 60);
 Clock = seconds;
 R_clock = Clock;
 TimerStop = false;
}

int ShowHours()
{
  return Clock / 3600;
}

int ShowMinutes()
{
  return (Clock / 60) % 60;
}

int ShowSeconds()
{
  return Clock % 60;
}

unsigned long ShowMilliSeconds()
{
  return (_micro - Watch)/ 1000.0;
}

unsigned long ShowMicroSeconds()
{
  return _micro - Watch;
}

boolean TimeHasChanged()
{
  return timeFlag;
}

// output true if timer equals requested time
boolean TimeCheck(unsigned int hours, unsigned int minutes, unsigned int seconds) 
{
  return (hours == ShowHours() && minutes == ShowMinutes() && seconds == ShowSeconds());
}
