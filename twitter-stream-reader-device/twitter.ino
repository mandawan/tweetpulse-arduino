

void launchTwitterStreaming(){
  // Kill all running python scripts if any
  p.runShellCommand(kill_command);
  // Start the python script
  // if we do have something to search for ...
  if(stringQuery != "")
  {
    Serial.print("We're searching for : ");
    Serial.println(stringQuery);    
    Serial.println(python_command);
    Serial.print("Launching search stream ...");     
    p.runShellCommandAsynchronously(python_command);
    Serial.println("done.");         
  }
}

void handleTwitterMentions()
{
  // read the "go" Bridge var and copies it into the go_buffer variable
  Bridge.get("go", go_buffer, 2);
  
  // convert the bugger into an int variable (should I have used bool instead?)
  go = atoi(go_buffer);
  
  //if we've received a GO signal
  if (go == 1){
    mentionned();
  } else {
    delay(50);
  }
}

void uploadRequest(String request, String lang) {
  // Write our shell script in /tmp
  // Using /tmp stores the script in RAM this way we can preserve 
  // the limited amount of FLASH erase/write cycles
  Serial.println(request);
  Serial.println(lang);
  p.runShellCommand("rm /tmp/newfile.txt");
  File script = FileSystem.open("/tmp/newfile.txt", FILE_WRITE);
  script.print(request+"\n");
  script.print(lang+"\n");
  script.close();  // close the file
  Serial.println("done writing twitter search to /tmp");
}


void mentionned()
{
  Serial.println("Mentionned !");
    
  mentionNum += 1;
  mentionCount += 1;  
  
  displayCount();
  
  ledblink_ms = ledblink_short_ms;
  
  // and resets the go variable to 0
  // you want to keep this line
  Bridge.put("go", String(0));
}  

