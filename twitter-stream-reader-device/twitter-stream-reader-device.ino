

/*
 * Twitter Stream mentions device

 The functions :
 * alert when a string is mentionned, whether it is a twitter login or a simple search criteria
 */



/*

TODO

(Mettre en place le push toutes les X minutes)
Mettre en place le blink permanent
Actions bouton
Acquisition MAC ADDRESS
Display setup

Check fonction twitter bridge
setup autres arduinos.

*/

#include <Bridge.h>
#include <BridgeServer.h>
#include <BridgeClient.h>
#include <TextFinder.h>
#include <LiquidCrystal.h>
#include <PubSubClient.h>
#include <FileIO.h>

#include <EEPROMAnything.h>
#include <EEPROM.h>
#include <SPI.h>
#include <avr/pgmspace.h>

#define WELCOME_STRING "Bienvenue cher utilisateur !"

#define CONFIG_VERSION "rt1"
#define CONFIG_START 32

#define MQTT_SERVER "test.mosquitto.org"
#define MQTT_PORT 1883

//-------------------------------
// RECHERCHE TWITTER PAR DEFAUT
//-------------------------------
//TODO enlever cette valeur par defaut
String stringQuery = "default";
String stringLang;
int mentionNum = 0;
int lastAverage = -1;
int mentionCount = 0;

// LED pin.
const int ledPin = A2;      // pin for the lef

const int button1Pin = A0;      // pin for the very left button
const int button2Pin = A1;    // pin for the middle left button

int lastButtonPushed = 0;

int lastButton1State = LOW;   // the previous reading from the button 1 input pin
int lastButton2State = LOW;   // the previous reading from the button 2 input pin

/*
MQTT VAR & POINTERS
*/
char clientId[7]      = "F30F0A";
char* pClientId      = clientId;
char dataTopicRoot[13]       = "uzful_data/";
char* pDataTopicRoot       = dataTopicRoot;
char controlTopicRoot[16]       = "uzful_control/";
char* pControlTopicRoot       = controlTopicRoot;

char dataTopicPath[12];
char controlTopicPath[12];
char* pDataTopicPath = dataTopicPath;
int sendToMQTTHours = 0;
int sendToMQTTMinutes = 0;
int sendToMQTTSeconds = 10;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
const long ledblink_long_ms = 2000;
const long ledblink_short_ms = 500;
long ledblink_ms = ledblink_long_ms;
long setupdisp_ms = 5000;
int ledState = LOW;             // ledState used to set the LED
long previousMillis = 0;        // will store last time LED was updated

/*
* Function prototypes to avoid "function not declared in scope" error
*/
void mqttCallback(char*, uint8_t*, unsigned int);

//HTTP client
BridgeClient client;
PubSubClient mqttClient(MQTT_SERVER, MQTT_PORT, mqttCallback, client);
String aMacAddress;
String aIPAddress;
String aLogin;

String response;

// configure this with your command path
// the go.py file must be in the same directory
// of the streaming.py file
const String SCRIPT_DIR = "/root/python/";

const String python_command = "/usr/bin/python " + SCRIPT_DIR + "streaming.py";
const String kill_command = "/usr/bin/python " + SCRIPT_DIR + "kill_processes.sh";

Process p;

char go_buffer[2];
// still not sure if I should have used a bool instead
// btw with int I can handle more states
int  go = 0;

void setup()
{
  delay(2000); //The bare minimum needed to be able to reboot both linino and leonardo.
  // if reboot fails try increasing this number
  // The more you run on linux the higher this number should be

  // hardware initialization: Either keep this or replace it with
  // your actual hardware configuration (i.e. servo)
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pins as inputs:
  pinMode(button1Pin, INPUT);
  pinMode(button2Pin, INPUT);



  Serial.begin(9600);
  while(!Serial);
  FileSystem.begin();

  Serial.print("Starting Bridge ...");
  // Initialize the Bridge Library
  Bridge.begin();
  Serial.println("done");

  //setAndPrintArduinoConfig();

  //lcd.begin(20,4);
  lcd.begin(20, 2);

  lcd.clear();
  lcd.print("// TweetPulse //");

  //Setup id du arduino et mix mqtt
  String strbuff;

  strbuff = String(dataTopicRoot) + String(clientId);//aLogin
  strbuff.toCharArray(dataTopicPath, strbuff.length() + 1);

  strbuff = String(controlTopicRoot) + String(clientId);
  strbuff.toCharArray(controlTopicPath, strbuff.length() + 1);
  
  connectMQTT();
  //TODO Push sur init/IDDELABOITE "init" ou whatever, le serveur répondra sur le même channel avec les datas dont j'ai besoin pour l'init

  
  // TEMP
  // on stocke la phrase dans un fichier texte.
  //uploadRequest(stringQuery, stringLang);


  //TEMP
  // on lance la recherche twitter
  //launchTwitterStreaming();

  //SetTimer(sendToMQTTHours,sendToMQTTMinutes,sendToMQTTSeconds);
  //StartTimer();
}

void loop()
{
 //handleInterrupts();
 //handleTimer();
 //handleTwitterMentions();

  /*if(connectMQTT()) */
  mqttClient.loop();
  
  //ledBlink();
}
