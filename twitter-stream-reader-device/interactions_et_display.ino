void handleInterrupts()
{
  // we check in wich state is each button, we store the information

  //TEMP en attendant de mettre une tempo sur ce bouton 1 avec un fade out auto
   if(!digitalRead(button1Pin)) displayCount();
  
  readButtons();
  
}

void  readButtons(){  //read buttons status
  int reading;
  int button1State=LOW;             // the current reading from the Enter input pin
  int button2State=LOW;             // the current reading from the input pin

  //button 1
                  // read the state of the switch into a local variable:
                  reading = digitalRead(button1Pin);

                  if(reading==HIGH)
                  {
                    //Serial.println("button1");
                    actionButton1();
                    return;
                  }                  

  //button 2
                  // read the state of the switch into a local variable:
                  reading = digitalRead(button2Pin);

                  if(reading==HIGH)
                  {
                    //Serial.println("button2");
                    actionButton2();
                  }      
}
void actionButton1() {
  displaySetup();
}

void actionButton2() {
  mentionCount = 0;
  displayCount();
}



void displaySetup(){
  lcd.clear(); 
  
  // remind the search string and the mentions count since last RESET
  lcd.setCursor(0, 0);
  lcd.print("Connect to arduino to setup wifi :");
  lcd.setCursor(0, 1);
  lcd.print("http://arduinoCTD"+aMacAddress.substring(15, aMacAddress.length()+1)+".local");
}
void displayCount(){
  char myStg[10];
  sprintf(myStg, "%d", mentionCount);

  lcd.clear(); 
  
  // remind the search string and the mentions count since last RESET
  lcd.setCursor(0, 0);
  
  lcd.print(String(myStg)+ " mentions !");
 
  lcd.setCursor(0, 1);
  lcd.print("pour '"+stringQuery.substring(0, 15)+"'");
}

//
// LED Heartbeat routine by Allen C. Huffman (www.appleause.com)
//
void ledBlink()
{
// here is where you'd put code that needs to be running all the time.

  // check to see if it's time to blink the LED; that is, if the 
  // difference between the current time and last time you blinked 
  // the LED is bigger than the interval at which you want to 
  // blink the LED.
  unsigned long currentMillis = millis();
 
  if(currentMillis - previousMillis > ledblink_ms) {
    // save the last time you blinked the LED 
    previousMillis = currentMillis;   

    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW)
      ledState = HIGH;
    else
      ledState = LOW;

    // set the LED with the ledState of the variable:
    digitalWrite(ledPin, ledState);
  }} // End of ledBlink()







//FEW UTILS
void clearMemory(){
  for (int i = 0; i < 1024; i++)
  EEPROM.write(i, 0);
}
//void getIPAdress(){
//  Process wifiCheck;  // initialize a new process
//  
//  wifiCheck.runShellCommand("/usr/bin/pretty-wifi-info.lua");  // command you want to run
//
//  // while there's any characters coming back from the
//  // process, print them to the serial monitor:
//  while (wifiCheck.available() > 0) {
//    char c = wifiCheck.read();
//    Serial.print(c);
//  }
//}
//

